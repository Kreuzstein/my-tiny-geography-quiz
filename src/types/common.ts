export const gameScreens = {
    menu: "menu",
    game: "game",
    records: "records",
} as const;

export type GameScreen = typeof gameScreens[keyof typeof gameScreens];