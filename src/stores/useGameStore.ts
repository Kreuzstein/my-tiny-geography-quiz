import { create } from "zustand";
import { devtools, persist } from "zustand/middleware";

import { gameScreens, type GameScreen } from "~/types/common";

interface GameProps {
    currentScreen: GameScreen;
};

interface GameReducers {
    navigate: (currentScreen: GameScreen) => void;
    reset: () => void;
};

type GameState = GameProps & GameReducers;

const defaults: GameProps = {
    currentScreen: gameScreens.menu,
};

export const useGameStore = create<GameState>()(
    devtools(
        persist(
            (set) => ({
                ...defaults,
                navigate: (currentScreen) => set((state) => ({
                    ...state,
                    currentScreen,
                })),
                reset: () => set((state) => ({
                    ...state,
                    ...defaults,
                })),
            }),
            {
                name: 'game-storage',
            },

        )
    )
);
