# My Tiny Geography Quiz

## About

"My Tiny Geography Quiz" is a progressive web application (PWA) specifically designed for geography trivia enthusiasts. It's a simple, straightforward quiz game that focuses on learning the shapes of different countries, including both sovereign and non-sovereign entities. 

The quiz is developed with Next.js and uses GeoJSON data for rendering the SVGs of different countries. There are a lot quizzes online that my mates and I play, but I often wished I could run them offline, like on the plane or when hiking. 

The project's goal is to works well in any situation - be it at home, in transit, or during any outdoor adventures.

### Running

TBA

### Production

TBA

## Licence

This work is distributed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Licence. 

## Contact

Anton Kreuzstein anton@quorum.ltd
