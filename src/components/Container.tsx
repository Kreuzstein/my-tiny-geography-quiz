import { type ReactNode } from "react";

type ContainerProps = {
    children: ReactNode
};

const Container: React.FC<ContainerProps> = ({ children }) => {
  return (
    <main className="flex h-screen flex-col items-center justify-center bg-base-100">
      <div className="mt-auto flex h-full w-full max-w-prose flex-col bg-neutral drop-shadow-2xl lg:h-4/5 lg:rounded-t-xl">
        {children}
      </div>
    </main>
  );
};

export default Container;
