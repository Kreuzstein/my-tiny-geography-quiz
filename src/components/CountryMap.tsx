/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { ComposableMap, Geographies, Geography, Graticule, Sphere } from "react-simple-maps";
import themes from "daisyui/src/theming/themes";
import { useEffect, useState } from "react";
import { geoProjection } from "d3-geo";

const GEOJSON = "./ne_10m_admin_0_countries.geojson";

const CountryMap = () => {
  // const countryIndex = 0;
  // const countryIndex = 2;
  // const countryIndex = 8;
  // const countryIndex = 255;
  // const countryIndex = 125;
  // const countryIndex = 55;
  // const countryIndex = 33;
  // const countryIndex = 52;
  // const countryIndex = 47;
  // const countryIndex = 77;
  // bad apples: Netherlands: 93, Russia: 47, Norway: 55 (tbc); San Marimo 111; Liechtenstein; Andorra 128; 133 guantanamo bay; brazil 139; gibraltar 153; US 154; e guinea 164; hk 166; vatican 167; 172 uk cyprus; antarctica 175; fiji 178; kiwi 179; aruba 185; st piere 190; pitcairn 192; fp 193 make THICKER; french aouth antractic 194; seychelles 195; kiribati 196; marsahlll thicker 197; grenada 199; st vincent 200;  barbados 201; st lucia 202; dominica 203; us outlying 204; montseraat 205; antigua 206; st kitts 207; us virgin 208; St-Barthélemy 209; anguilla 211; uk virgin 212; bermuda 215; herald 216; st helea thivker 217; maurities 218; malta 222; jersey 223; guernsey 224; indion oc terr 228; uk indian oc 229; spore 230; norfilk 231; cook 232; samoa 235; tuvalu 237; nauru 239; niue 244; us samoa 245; guam 247; bahrain 249; coral sea 250; Clipperton 252; macau 253; Ashmore and Cartier Islands 254; baj nuevo 255; Serranilla Bank 256; scarrborough reef 257;  st maaerten 37 38; nicaragua late 29; france 21;  
  // monaco is 120 maybe look for a geosjon with diff handling of martitime borders?
  const width = 800;
  const height = 800;

  const [countryIndex, setCounryIndex] = useState(54);
  const [geoData, setGeoData] = useState(null);
  const [[xCentre, yCentre], setCentre] = useState([0, 0]);
  const DEFAULT_SCALE_FACTOR = 100;
  const [scaleFactor, setScaleFactor] = useState(DEFAULT_SCALE_FACTOR);
  const [countryName, setCountryName] = useState("");
  const [aspectRatio, setAspectRatio] = useState(1);
  // const [myProjection, setProjection] = useState(() =>
  //   geoProjection((x, y) => [x, y]),
  // );
  useEffect(() => {
    fetch(GEOJSON)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((jsonData) => {
        const country = jsonData.features[countryIndex];
        const [x0, y0, x1, y1] = country.bbox;
        setCentre([x0 + (x1 - x0) / 2, y0 + (y1 - y0) / 2]);
        const [xLen, yLen] = [Math.abs(x1 - x0), Math.abs(y1 - y0)];
        setAspectRatio(yLen / xLen);
        console.log(countryIndex);
        console.log(country);
        console.log(countryName);
        setScaleFactor(
          xLen >= yLen * 1.45
            ? (DEFAULT_SCALE_FACTOR * 360) / xLen
            : (DEFAULT_SCALE_FACTOR * 360) / yLen,
            // : (DEFAULT_SCALE_FACTOR * (360 - Math.abs(yCentre))) / yLen,
            // : (DEFAULT_SCALE_FACTOR * (Math.abs(yCentre) + 180)) / yLen,
            // : (DEFAULT_SCALE_FACTOR * (Math.abs(yCentre) > 54 ? 360 : 180)) / yLen,
            // : (DEFAULT_SCALE_FACTOR * (Math.abs(yCentre) > 45 ? 360 : 180)) / yLen,
        );
        setCountryName(country.properties.NAME_SORT);
        // setProjection(() =>
        //   geoProjection((x, y) => [x, y])
        //     .translate([width / 2, height / 2])
        //     .center([xCentre, yCentre])
        //     .scale(scaleFactor),
        // );
        setGeoData(jsonData);
      })
      .catch((error) => {
        console.error("Error in fetching the geoJson:", error);
      });
  }, [countryIndex]);

  return (
    <div className="flex w-full flex-col">
      <ComposableMap
        height={aspectRatio < 1.15 ? width * aspectRatio * 1.15 : width}
        width={width}
        projection={"geoMercator"}
        projectionConfig={{
          // center: [xCentre, yCentre],
          scale: scaleFactor > 100 ? scaleFactor : 100,
          rotate: [-xCentre, -yCentre, 0]
        }}
      >
      {/* <Graticule stroke="#999" /> */}
      {/* <Sphere id="sphere" fill="none" stroke="#06F" strokeWidth={2} /> */}

        <Geographies geography={geoData}>
          {({ geographies }) => {
            const country = geographies[countryIndex];
            return (
              <Geography
                key={country.rsmKey}
                geography={country}
                fill={themes["[data-theme=synthwave]"].accent}
                // fill="#FFFFFF00"
                stroke={themes["[data-theme=synthwave]"].accent}
              />
            );
          }}
        </Geographies>
      </ComposableMap>
      <div className="mx-auto text-neutral hover:text-white">{countryName}</div>
      <div className="mx-auto space-x-3">
        <button
          className="p3 btn btn-accent btn-sm mx-auto my-3"
          onClick={() => setCounryIndex(countryIndex - 1)}
        >
          back
        </button>
        <button
          className="p3 btn btn-accent btn-sm mx-auto my-3"
          onClick={() => setCounryIndex(countryIndex + 1)}
        >
          next
        </button>
      </div>
    </div>
  );
};

export default CountryMap;
