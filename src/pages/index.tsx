import Head from "next/head";

import { gameScreens } from "~/types/common";

import { useGameStore } from "~/stores/useGameStore";

import Container from "~/components/Container";
import Game from "~/components/game/Game";
import MainMenu from "~/components/mainMenu/MainMenu";
// import Link from "next/link";

// import { api } from "~/utils/api";

export default function Home() {
  // const hello = api.example.hello.useQuery({ text: "from tRPC" });
  // min-h-screen
  const gameState = useGameStore();
  return (
    <>
      <Head>
        <title>My Tiny Geography Quiz</title>
        <meta name="description" content="Brought to you by quorum.ltd" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#221551" />
        <meta name="msapplication-TileColor" content="#221551" />
        <meta name="theme-color" content="#221551" />
      </Head>
      <Container>
        {(() => {
          switch (gameState.currentScreen) {
            case gameScreens.game:
              return <Game />;

            default:
              return <MainMenu />;
          }
        })()}
      </Container>
    </>
  );
}
