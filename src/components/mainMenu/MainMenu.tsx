import themes from "daisyui/src/theming/themes";

import Logo from "../icons/Logo";

const MainMenu: React.FC = () => {
  return (
    <>
      <div className="mx-auto py-24 scale-[2.5]">
        <Logo
          bg={themes["[data-theme=synthwave]"].neutral}
          accent={themes["[data-theme=synthwave]"].accent}
        />
      </div>
      <div className="mx-auto font-header text-2xl text-primary">
        My Tiny Geography Quiz
      </div>
    </>
  );
};

export default MainMenu;
