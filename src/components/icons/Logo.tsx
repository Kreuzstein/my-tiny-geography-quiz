type Props = {
  bg?: string;
  accent?: string;
};

const Icon: React.FC<Props> = (props) => {
  return (
    <svg
      width="64"
      height="64"
      viewBox="0 0 64 64"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clip-path="url(#clip0_9_133)">
        <rect width="64" height="64" fill={props.bg} />
        <path
          d="M32 2.5H31H33M2.5 31V33M2.65066 35C4.05425 48.8969 15.1031 59.9458 29 61.3493M2.65066 29C4.05425 15.1031 15.1031 4.05425 29 2.65066M35 2.65066C48.8969 4.05425 59.9458 15.1031 61.3493 29M61.3493 35C59.9457 48.8969 48.8969 59.9457 35 61.3493M61.5 31V32V33M33 61.5H32H31"
          stroke={props.accent}
          stroke-width="2"
        />
        <path
          d="M32 24V5L25.8286 25.8286L27.6387 25.2922C28.8932 24.4749 30.3911 24 32 24Z"
          fill={props.accent}
        />
        <path
          d="M32 24C30.3911 24 28.8932 24.4749 27.6387 25.2922L32 24Z"
          fill={props.accent}
        />
        <path
          d="M32 40V59L38.1714 38.1714L36.3613 38.7078C35.1068 39.5251 33.6089 40 32 40Z"
          fill={props.accent}
        />
        <path
          d="M36.3613 38.7078L32 40C33.6089 40 35.1068 39.5251 36.3613 38.7078Z"
          fill={props.accent}
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32 24V32L26.3431 26.3431C26.7365 25.9498 27.1707 25.5972 27.6387 25.2922L32 24Z"
          fill={props.accent}
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M27.6387 25.2922C27.1707 25.5972 26.7365 25.9498 26.3431 26.3431L25.8286 25.8286L27.6387 25.2922Z"
          fill={props.accent}
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M38.1714 38.1714L36.3613 38.7078C36.8293 38.4028 37.2635 38.0502 37.6569 37.6569L38.1714 38.1714Z"
          fill={props.accent}
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32 40V32L37.6569 37.6569C37.2635 38.0502 36.8293 38.4028 36.3613 38.7078L32 40Z"
          fill={props.accent}
        />
        <path
          d="M32 32H40L38.7078 27.6387C38.4028 27.1707 38.0502 26.7365 37.6569 26.3431L32 32Z"
          fill={props.accent}
        />
        <path
          d="M38.7078 27.6387L38.1714 25.8286L37.6569 26.3431C38.0502 26.7365 38.4028 27.1707 38.7078 27.6387Z"
          fill={props.accent}
        />
        <path
          d="M40 32C40 30.3911 39.5251 28.8932 38.7078 27.6387L40 32Z"
          fill={props.accent}
        />
        <path
          d="M40 32H59L38.1714 25.8286L38.7078 27.6387C39.5251 28.8932 40 30.3911 40 32Z"
          fill={props.accent}
        />
        <path
          d="M32 32H24L25.2922 36.3613C25.3223 36.4075 25.3529 36.4533 25.3839 36.4988C25.6671 36.9146 25.9886 37.3023 26.3431 37.6569L32 32Z"
          fill={props.accent}
        />
        <path
          d="M25.3839 36.4988C25.3529 36.4533 25.3223 36.4075 25.2922 36.3613L25.8286 38.1714L26.3431 37.6569C25.9886 37.3023 25.6671 36.9146 25.3839 36.4988Z"
          fill={props.accent}
        />
        <path
          d="M25.2922 36.3613L24 32C24 33.6089 24.4749 35.1068 25.2922 36.3613Z"
          fill={props.accent}
        />
        <path
          d="M24 32H5L25.8286 38.1714L25.2922 36.3613C24.4749 35.1068 24 33.6089 24 32Z"
          fill={props.accent}
        />
        <path
          d="M40 32H32M40 32H59M40 32C40 30.3911 39.5251 28.8932 38.7078 27.6387M40 32L38.7078 27.6387M32 40L36.3613 38.7078M32 40V32M32 40C33.6089 40 35.1068 39.5251 36.3613 38.7078M32 40V59M24 32H32M24 32H5M24 32L25.2922 36.3613M24 32C24 33.6089 24.4749 35.1068 25.2922 36.3613M32 24V32M32 24C30.3911 24 28.8932 24.4749 27.6387 25.2922M32 24L27.6387 25.2922M32 24V5M32 5V32M32 5L38.1714 25.8286M32 5L25.8286 25.8286M32 32V59M32 32L26.3431 26.3431M32 32L37.6569 37.6569M32 32L37.6569 26.3431M32 32L26.3431 37.6569M32 59L38.1714 38.1714M32 59L25.8286 38.1714M5 32L25.8286 25.8286M5 32L25.8286 38.1714M59 32L38.1714 25.8286M59 32L38.1714 38.1714M27.6387 25.2922L25.8286 25.8286M27.6387 25.2922C27.1707 25.5972 26.7365 25.9498 26.3431 26.3431M38.7078 27.6387L38.1714 25.8286M38.7078 27.6387C38.4028 27.1707 38.0502 26.7365 37.6569 26.3431M38.1714 25.8286L37.6569 26.3431M36.3613 38.7078L38.1714 38.1714M36.3613 38.7078C36.8293 38.4028 37.2635 38.0502 37.6569 37.6569M25.2922 36.3613C25.3223 36.4075 25.3529 36.4533 25.3839 36.4988C25.6671 36.9146 25.9886 37.3023 26.3431 37.6569M25.2922 36.3613L25.8286 38.1714M25.8286 25.8286L26.3431 26.3431M38.1714 38.1714L37.6569 37.6569M25.8286 38.1714L26.3431 37.6569"
          stroke={props.accent}
          stroke-width="0.5"
        />
      </g>
      <defs>
        <clipPath id="clip0_9_133">
          <rect width="64" height="64" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

Icon.defaultProps = { bg: "#221551", accent: "#FECF0B" };

export default Icon;
