import CountryMap from "./CountryMap";

const Game: React.FC = () => {
  return (
    <>
      <div className="mx-auto p-6 font-header text-xl text-primary">
        My Tiny Geography Quiz
      </div>
      <div className="w-full border border-secondary"></div>
      <div className="mx-auto flex pt-6 font-body text-primary">
        What is this country?
      </div>
      <div className="mx-auto flex max-h-[65%] w-full p-6">
        <CountryMap />
      </div>
    </>
  );
};

export default Game;
